db.fruits.insertMany([
    {
      name : "Apple",
      color : "Red",
      stock : 20,
      price: 40,
      supplier_id : 1,
      onSale : true,
      origin: [ "Philippines", "US" ]
    },
  
    {
      name : "Banana",
      color : "Yellow",
      stock : 15,
      price: 20,
      supplier_id : 2,
      onSale : true,
      origin: [ "Philippines", "Ecuador" ]
    },
  
    {
      name : "Kiwi",
      color : "Green",
      stock : 25,
      price: 50,
      supplier_id : 1,
      onSale : true,
      origin: [ "US", "China" ]
    },
  
    {
      name : "Mango",
      color : "Yellow",
      stock : 10,
      price: 120,
      supplier_id : 2,
      onSale : false,
      origin: [ "Philippines", "India" ]
    }
  ]);

// Use the count operator to count the total number of fruits in sale.

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$count: "onSale"}
]);

// Use count operator to count the total number of fruits with stock more than 20.

db.fruits.aggregate([
  {$project: {
         stocks: { $gt: [ "$stocks", 250 ] }
       }
  },
  {$count: "stock"}
]);

// Use th average operator to get the average proce of fruits onsale per supplier.

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {
    _id: "supplier_id",
    avg_price: {$avg: "$price"}
  }}
]);


// Use th $max operator to get the average proce of fruits onsale per supplier.


db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {
    _id: "supplier_id",
    max_price: {$max: "$price"}
  }}
]);

// Use th $min operator to get the average proce of fruits onsale per supplier.


db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {
    _id: "supplier_id",
    min_price: {$min: "$price"}
  }}
]);

